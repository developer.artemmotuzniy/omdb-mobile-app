package com.artemmotuzniy.softindustrytest.data.remote

import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MovieApi {

	@GET("/")
	fun getMoviesAsync(@QueryMap params: HashMap<String, Any>): Deferred<SearchMovieResponse>

}