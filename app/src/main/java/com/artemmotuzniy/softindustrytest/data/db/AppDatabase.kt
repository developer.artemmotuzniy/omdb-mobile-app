package com.artemmotuzniy.softindustrytest.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.artemmotuzniy.softindustrytest.data.model.Movie

@Database(entities = [Movie::class], version = 2)
abstract class AppDatabase : RoomDatabase() {
    abstract fun favouriteMoviesDao(): FavouriteMoviesDao
}