package com.artemmotuzniy.softindustrytest.data

import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResult

interface MoviesRepository {

	suspend fun getFavouriteMovies(offset: Int): List<Movie>

	suspend fun addMovieToFavourite(movie: Movie): Movie

	suspend fun removeMovieFromFavourite(movie: Movie): Movie

	suspend fun getMoviesWithFavourites(search: String, page: Int): SearchMovieResult


}