package com.artemmotuzniy.softindustrytest.data.db

import androidx.room.*
import com.artemmotuzniy.softindustrytest.data.model.Movie

@Dao
interface FavouriteMoviesDao {

	@Query("SELECT * from movies LIMIT 10 OFFSET :offset")
	fun getFavouriteMovies(offset: Int): List<Movie>

	@Query("SELECT * FROM movies WHERE imdbID = :imdbId LIMIT 1")
	fun getFavouriteMovieById(imdbId: String?): Movie?

	@Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie): Long

    @Delete
    fun delete(movie: Movie): Int
}