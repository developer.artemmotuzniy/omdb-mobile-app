package com.artemmotuzniy.softindustrytest.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movies")
data class Movie(@PrimaryKey(autoGenerate = true) var id: Long? = null,
				 @SerializedName("imdbID") val imdbID: String? = null,
				 @SerializedName("Title") val title: String? = null,
				 @SerializedName("Year") val year: String? = null,
				 @SerializedName("Type") val type: String? = null,
				 @SerializedName("Poster") val poster: String? = null,
				 var favourite: Boolean = false,
				 var rating: Float? = null) : Parcelable {

	constructor(parcel: Parcel) : this(
			parcel.readValue(Long::class.java.classLoader) as? Long,
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readByte() != 0.toByte(),
			parcel.readValue(Float::class.java.classLoader) as? Float)

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeValue(id)
		parcel.writeString(imdbID)
		parcel.writeString(title)
		parcel.writeString(year)
		parcel.writeString(type)
		parcel.writeString(poster)
		parcel.writeByte(if (favourite) 1 else 0)
		parcel.writeValue(rating)
	}

	override fun describeContents(): Int {
		return 0
	}

	fun hasPoster() = poster != "N/A"

	companion object CREATOR : Parcelable.Creator<Movie> {
		override fun createFromParcel(parcel: Parcel): Movie {
			return Movie(parcel)
		}

		override fun newArray(size: Int): Array<Movie?> {
			return arrayOfNulls(size)
		}
	}
}