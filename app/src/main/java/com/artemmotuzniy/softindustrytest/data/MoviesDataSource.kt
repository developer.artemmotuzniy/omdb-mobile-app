package com.artemmotuzniy.softindustrytest.data

import com.artemmotuzniy.softindustrytest.*
import com.artemmotuzniy.softindustrytest.data.db.FavouriteMoviesDao
import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResponse
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResult
import com.artemmotuzniy.softindustrytest.data.remote.MovieApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MoviesDataSource(private val dao: FavouriteMoviesDao,
					   private val service: MovieApi,
					   private val apiKey: String) : MoviesRepository {

	companion object {
		private const val SEARCH_TYPE = "movie"
	}

	override suspend fun getFavouriteMovies(offset: Int): List<Movie> {
		val movies = dao.getFavouriteMovies(offset)
		return if (movies.isNotEmpty()) movies else throw EmptyListException()
	}

	override suspend fun addMovieToFavourite(movie: Movie): Movie {
		movie.favourite = true
		val id = dao.insert(movie)
		if (id != -1L) {
			movie.id = id
		}
		return movie
	}

	override suspend fun removeMovieFromFavourite(movie: Movie): Movie {
		val id = dao.delete(movie)
		if (id != -1) {
			movie.favourite = false
			movie.id = null
			movie.rating = null
		}
		return movie
	}

	override suspend fun getMoviesWithFavourites(search: String, page: Int): SearchMovieResult {
		val searchResponse = loadFromApi(search, page).await()
		if (searchResponse.error == null) {
			return generateSearchResult(searchResponse)
		} else {
			throw generateSearchException(searchResponse.error)
		}
	}

	private fun generateSearchException(error: String) = when (error) {
		TOO_MANY_RESULT -> TooManyResultException()
		MOVIE_NOT_FOUNT -> MovieNotFoundException()
		else -> Exception()
	}

	private fun generateSearchResult(searchResponse: SearchMovieResponse): SearchMovieResult {
		val remoteWithFavourite: List<Movie> = searchResponse.movies?.asSequence()?.map { movie ->
			movie.takeIf { it.id == null }?.let { dao.getFavouriteMovieById(movie.imdbID) } ?: movie
		}?.toList() ?: throw EmptyListException()

		return SearchMovieResult(remoteWithFavourite, searchResponse.totalResults ?: 0)
	}

	private suspend fun loadFromApi(search: String, page: Int) = withContext(Dispatchers.IO) {
		if (!search.isBlank()) {
			val body: HashMap<String, Any> = hashMapOf(
					"s" to search,
					"page" to page,
					"type" to SEARCH_TYPE,
					"apikey" to apiKey)

			service.getMoviesAsync(body)
		} else {
			throw EmptyListException()
		}
	}
}