package com.artemmotuzniy.softindustrytest.data.model

import com.google.gson.annotations.SerializedName

data class SearchMovieResponse(@SerializedName("Search") val movies: List<Movie>?,
							   val totalResults: Int?,
							   @SerializedName("Error")
							   val error: String?)

class SearchMovieResult(val movies: List<Movie>,
						val totalResults: Int)