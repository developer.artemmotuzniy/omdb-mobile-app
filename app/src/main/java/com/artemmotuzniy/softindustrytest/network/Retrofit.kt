package com.artemmotuzniy.softindustrytest.network

import android.content.Context
import android.net.ConnectivityManager
import com.artemmotuzniy.softindustrytest.InternetConnectionException
import com.artemmotuzniy.softindustrytest.R
import com.artemmotuzniy.softindustrytest.data.remote.MovieApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class Retrofit(private val context: Context) {

    private fun httpClient() = with(OkHttpClient.Builder()) {
        addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
		addInterceptor(InternetConnectionInterceptor(context))
        build()
    }

    fun apiService(): MovieApi = with(Retrofit.Builder()) {
        baseUrl(context.getString(R.string.base_api))
		addCallAdapterFactory(CoroutineCallAdapterFactory())
        addConverterFactory(GsonConverterFactory.create())
        client(httpClient())
        build()
    }.create(MovieApi::class.java)
}

class InternetConnectionInterceptor(private val context: Context) : Interceptor {
	override fun intercept(chain: Interceptor.Chain): Response {
		if (!isOnline(context)) {
			throw InternetConnectionException()
		}
		return chain.proceed(chain.request().newBuilder().build())
	}

	private fun isOnline(context: Context): Boolean {
		val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
		val netInfo = connectivityManager?.activeNetworkInfo
		return netInfo != null && netInfo.isConnected
	}
}