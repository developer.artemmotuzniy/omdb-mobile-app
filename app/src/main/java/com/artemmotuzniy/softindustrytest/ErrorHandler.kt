package com.artemmotuzniy.softindustrytest

import com.artemmotuzniy.softindustrytest.base.BaseView
import com.artemmotuzniy.softindustrytest.base.view.ErrorView
import com.artemmotuzniy.softindustrytest.base.view.InternetConnectionView
import com.artemmotuzniy.softindustrytest.base.view.MovieListView
import com.artemmotuzniy.softindustrytest.searchmovies.SearchMoviesView
import java.io.IOException

const val MOVIE_NOT_FOUNT = "Movie not found!"
const val TOO_MANY_RESULT = "Too many results."

fun handleError(view: BaseView?, e: Exception) {
    when {
        e is InternetConnectionException && view is InternetConnectionView? -> view?.internetConnectionError()
        e is MovieNotFoundException && view is SearchMoviesView? -> view?.movieNotFoundError()
        e is TooManyResultException && view is SearchMoviesView? -> view?.tooManyResultError()
        e is EmptyListException && view is MovieListView? -> view?.emptyError()
        view is ErrorView? -> view?.showError()
    }
}

class InternetConnectionException : IOException()

class MovieNotFoundException : IOException(MOVIE_NOT_FOUNT)

class TooManyResultException : IOException(TOO_MANY_RESULT)

class EmptyListException : IOException()