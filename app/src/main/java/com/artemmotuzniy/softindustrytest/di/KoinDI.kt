package com.artemmotuzniy.softindustrytest.di

import android.content.Context
import androidx.room.Room
import com.artemmotuzniy.softindustrytest.R
import com.artemmotuzniy.softindustrytest.data.MoviesDataSource
import com.artemmotuzniy.softindustrytest.data.MoviesRepository
import com.artemmotuzniy.softindustrytest.data.db.AppDatabase
import com.artemmotuzniy.softindustrytest.favouritesmovies.FavouritePresenter
import com.artemmotuzniy.softindustrytest.favouritesmovies.FavouritesDataSource
import com.artemmotuzniy.softindustrytest.favouritesmovies.FavouritesPresenterImpl
import com.artemmotuzniy.softindustrytest.favouritesmovies.FavouritesRepository
import com.artemmotuzniy.softindustrytest.network.Retrofit
import com.artemmotuzniy.softindustrytest.searchmovies.SearchDataSource
import com.artemmotuzniy.softindustrytest.searchmovies.SearchMoviesPresenter
import com.artemmotuzniy.softindustrytest.searchmovies.SearchMoviesPresenterImpl
import com.artemmotuzniy.softindustrytest.searchmovies.SearchRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

val appModule = module {
	single { Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "database")
			.fallbackToDestructiveMigration()
			.build() }
	single { get<AppDatabase>().favouriteMoviesDao() }
	single { Retrofit(get()).apiService() }
	single<MoviesRepository> { MoviesDataSource(get(), get(),get<Context>().getString(R.string.apikey)) }
}

val searchMovieModule = module {
	single<SearchRepository> { SearchDataSource(get()) }
	factory<SearchMoviesPresenter> { SearchMoviesPresenterImpl(get()) }
}

val favouriteMoviesModule = module {
	single<FavouritesRepository> { FavouritesDataSource(get()) }
	factory<FavouritePresenter> { FavouritesPresenterImpl(get()) }
}