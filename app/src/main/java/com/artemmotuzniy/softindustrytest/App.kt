package com.artemmotuzniy.softindustrytest

import android.app.Application
import androidx.room.Room
import com.artemmotuzniy.softindustrytest.data.db.AppDatabase
import com.artemmotuzniy.softindustrytest.di.appModule
import com.artemmotuzniy.softindustrytest.di.favouriteMoviesModule
import com.artemmotuzniy.softindustrytest.di.searchMovieModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    lateinit var database: AppDatabase

    override fun onCreate() {
        super.onCreate()

        startKoin(applicationContext, listOf(appModule, searchMovieModule, favouriteMoviesModule))

        database = Room.databaseBuilder(this, AppDatabase::class.java, "database")
            .build()
    }


}
