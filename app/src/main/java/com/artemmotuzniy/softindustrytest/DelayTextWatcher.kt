package com.artemmotuzniy.softindustrytest

import androidx.appcompat.widget.SearchView
import kotlinx.coroutines.*

class DelayTextWatcher(private val delay: Long, private val onTextChange: (String) -> Unit = {}) : SearchView.OnQueryTextListener {

	private val delayScope = CoroutineScope(Dispatchers.Main + Job())
	private var delayJob: Job? = null

	override fun onQueryTextSubmit(query: String?): Boolean {
		return false
	}

	override fun onQueryTextChange(newText: String?): Boolean {
		launch(newText)
		return true
	}

	private fun launch(text: String?) {
		if (text != null) {
			delayJob?.cancel()
			delayJob = delayScope.launch {
				delay(delay)
				onTextChange(text)
			}
		}
	}
}