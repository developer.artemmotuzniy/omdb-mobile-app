package com.artemmotuzniy.softindustrytest

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.artemmotuzniy.softindustrytest.data.model.Movie

class RemovingFavouriteDialog : DialogFragment() {


	companion object {
		private const val TAG = "RemovingFavouriteDialog"

		private const val KEY_MOVIE = "key_movie"

		fun newInstance(movie: Movie, onConfirmRemoving: () -> Unit): RemovingFavouriteDialog {
			return RemovingFavouriteDialog().apply {
				arguments = Bundle().apply {
					putParcelable(KEY_MOVIE, movie)
				}

				this.onConfirmRemoving = onConfirmRemoving
			}
		}
	}

	private var onConfirmRemoving: (() -> Unit)? = null

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val movie = arguments?.getParcelable<Movie>(KEY_MOVIE)
		return if (movie != null) createDialog(movie) else super.onCreateDialog(savedInstanceState)
	}

	private fun createDialog(movie: Movie): Dialog {
		return with(AlertDialog.Builder(requireContext())) {
			setTitle(R.string.remote_favourite_title)
			setMessage(movie.title)
			setNegativeButton(android.R.string.cancel) { dialog, _ ->
				dialog.cancel()
			}
			setPositiveButton(android.R.string.ok) { dialog, _ ->
				onConfirmRemoving?.invoke()
				dialog.dismiss()
			}
			show()
		}
	}

	fun show(childFragmentManager: FragmentManager) {
		super.show(childFragmentManager, TAG)
	}
}