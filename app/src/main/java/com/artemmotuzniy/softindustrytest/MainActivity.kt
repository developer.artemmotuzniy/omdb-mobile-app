package com.artemmotuzniy.softindustrytest

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.artemmotuzniy.softindustrytest.base.view.PageView
 import com.artemmotuzniy.softindustrytest.favouritesmovies.FavouriteFragment
import com.artemmotuzniy.softindustrytest.searchmovies.SearchMoviesFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

	private var adapter: MainPagerAdapter? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		main_toolbar.setTitle(R.string.app_name)
		setSupportActionBar(main_toolbar)

		adapter = MainPagerAdapter(supportFragmentManager, listOf(
				SearchMoviesFragment() to getString(R.string.search_tab_title),
				FavouriteFragment() to getString(R.string.favourites_tab_title)
		)).also { main_viewpager.adapter = it }


		main_viewpager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
			override fun onPageSelected(position: Int) {
				updateMoviesList(position)
				invalidateOptionsMenu()
			}
		})
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.menu_search, menu)
		menu?.findItem(R.id.menu_action_search)?.isVisible = main_viewpager.currentItem == 0
		return super.onCreateOptionsMenu(menu)
	}


	private fun updateMoviesList(position: Int) {
		val fragment = adapter?.getItem(position)
		if (fragment is PageView?) {
			fragment?.visible()
		}
	}
}

private class MainPagerAdapter(fm: FragmentManager, val fragments: List<Pair<Fragment, String>>) : FragmentPagerAdapter(fm) {

	override fun getItem(position: Int): Fragment {
		return fragments[position].first
	}

	override fun getCount(): Int {
		return fragments.size
	}

	override fun getPageTitle(position: Int): CharSequence? {
		return fragments[position].second
	}
}
