package com.artemmotuzniy.softindustrytest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_movie.view.*


class MoviesAdapter(private val onFavouriteClick: (Movie) -> Unit) : RecyclerView.Adapter<MoviesAdapter.MovieHolder>() {

	private enum class Payload { MOVIE_CHANGE }

	private val movieList = mutableListOf<Movie>()

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
		val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
		return MovieHolder(view)
	}

	override fun getItemCount(): Int = movieList.size

	override fun onBindViewHolder(holder: MovieHolder, position: Int, payloads: MutableList<Any>) {
		if (payloads.isNotEmpty()) {
			when (payloads[0]) {
				Payload.MOVIE_CHANGE -> {
					holder.bind(movieList[position], onFavouriteClick)
				}
			}
		}

		super.onBindViewHolder(holder, position, payloads)
	}

	override fun onBindViewHolder(holder: MovieHolder, position: Int) {
		holder.bind(movieList[position], onFavouriteClick)
	}

	fun addMovies(movies: List<Movie>) {
		notifyItemRangeInserted(movieList.size, movies.size)
		movieList.addAll(movies)
	}

	fun updateMovie(movie: Movie) {
		val index = movieList.indexOf(movie)
		movieList[index] = movie
		notifyItemChanged(index, Payload.MOVIE_CHANGE)
	}

	fun setMovies(movies: List<Movie>) {
		movieList.clear()
		movieList.addAll(movies)
		notifyDataSetChanged()
	}

	fun remove(movie: Movie) {
		val index = movieList.indexOf(movie)
		movieList.remove(movie)
		notifyItemRemoved(index)

	}

	class MovieHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

		fun bind(movie: Movie, onFavouriteClick: (Movie) -> Unit) {
			with(itemView) {
				item_movie_title.text = movie.title
				item_movie_year.text = movie.year
				itemView.item_movie_favourite.isSelected = movie.favourite

				showRating(movie.rating)

				itemView.item_movie_favourite.setOnClickListener {
					onFavouriteClick(movie)
				}

				val poster = if (movie.hasPoster()) movie.poster else R.drawable.ic_default_poster

				Glide.with(this)
						.load(movie.poster)
						.apply(RequestOptions.placeholderOf(R.drawable.ic_default_poster))
						.into(item_movie_poster)
			}
		}

		private fun showRating(rating: Float?) {
			if (rating != null) {
				itemView.item_movie_rating_group.visibility = View.VISIBLE
				itemView.item_movie_rating.text = rating.toString()
			} else {
				itemView.item_movie_rating_group.visibility = View.GONE
			}
		}
	}
}