package com.artemmotuzniy.softindustrytest.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel

interface BaseView

interface BasePresenter<T : BaseView> {

    var view: T?
    var scope: CoroutineScope?

    fun attach(view: T) {
        this.view = view
		this.scope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    }

    fun detach() {
        this.view = null
        this.scope?.cancel()
		this.scope = null
    }

}