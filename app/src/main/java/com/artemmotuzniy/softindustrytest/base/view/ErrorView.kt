package com.artemmotuzniy.softindustrytest.base.view

interface ErrorView {

	fun showError()

}