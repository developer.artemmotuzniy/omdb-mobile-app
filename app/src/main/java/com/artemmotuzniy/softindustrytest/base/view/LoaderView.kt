package com.artemmotuzniy.softindustrytest.base.view

interface LoaderView {

	fun showLoader()

	fun hideLoader()

}