package com.artemmotuzniy.softindustrytest.base.view

import com.artemmotuzniy.softindustrytest.data.model.Movie

interface MovieListView {

    fun setData(movies: List<Movie>)

    fun addData(movies: List<Movie>)

    fun emptyError()
}