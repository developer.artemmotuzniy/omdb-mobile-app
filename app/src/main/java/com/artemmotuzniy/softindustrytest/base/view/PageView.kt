package com.artemmotuzniy.softindustrytest.base.view

interface PageView {

	fun visible()

}