package com.artemmotuzniy.softindustrytest.searchmovies

import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResult

interface SearchRepository {

    suspend fun addMovieToFavourite(movie: Movie): Movie

    suspend fun removeMovieFromFavourite(movie: Movie): Movie

    suspend fun getMoviesWithFavourites(search: String, page: Int): SearchMovieResult

}