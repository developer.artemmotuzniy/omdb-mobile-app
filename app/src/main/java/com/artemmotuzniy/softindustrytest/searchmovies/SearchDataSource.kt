package com.artemmotuzniy.softindustrytest.searchmovies

import com.artemmotuzniy.softindustrytest.data.MoviesRepository
import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResult

class SearchDataSource(private val moviesRepository: MoviesRepository) : SearchRepository {

    override suspend fun addMovieToFavourite(movie: Movie): Movie {
        return moviesRepository.addMovieToFavourite(movie)
    }

    override suspend fun removeMovieFromFavourite(movie: Movie): Movie {
        return moviesRepository.removeMovieFromFavourite(movie)
    }

    override suspend fun getMoviesWithFavourites(search: String, page: Int): SearchMovieResult {
        return moviesRepository.getMoviesWithFavourites(search, page)
    }
}