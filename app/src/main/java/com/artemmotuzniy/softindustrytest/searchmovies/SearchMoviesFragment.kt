package com.artemmotuzniy.softindustrytest.searchmovies

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.artemmotuzniy.softindustrytest.*
import com.artemmotuzniy.softindustrytest.data.model.Movie
import kotlinx.android.synthetic.main.fragment_movies.*
import org.koin.android.ext.android.inject

class SearchMoviesFragment : Fragment(), SearchMoviesView {

	companion object {
		private const val SEARCH_DELAY_MS = 500L
	}

	private val presenter: SearchMoviesPresenter by inject()
	private val adapter = MoviesAdapter {
		presenter.updateMovie(it)
	}

	private var searchMenuItem: MenuItem? = null
	private var searchView: SearchView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
		setHasOptionsMenu(true)
		presenter.attach(this)

		search_movies_refresh.setOnRefreshListener {
			presenter.loadMovies(true)
		}

		search_movies_list.layoutManager = LinearLayoutManager(requireContext())
		search_movies_list.adapter = adapter
		search_movies_list.setUpPagination {
			presenter.loadMovies(false)
		}

		search_movies_info.setText(R.string.error_empty)
	}

	override fun onDestroyView() {
		super.onDestroyView()
		presenter.detach()
	}

	override fun addData(movies: List<Movie>) {
		adapter.addMovies(movies)
	}

	override fun updateMovie(movie: Movie) {
		adapter.updateMovie(movie)
	}

	override fun setData(movies: List<Movie>) {
		search_movies_info.visibility = View.GONE
		search_movies_list.visibility = View.VISIBLE

		adapter.setMovies(movies)
	}

	override fun showLastQuery(currentQuery: String) {
		searchView?.setQuery(currentQuery, false)
	}

	override fun showLoader() {
		search_movies_refresh.isRefreshing = true
	}

	override fun hideLoader() {
		search_movies_refresh.isRefreshing = false
	}

	override fun showError() {
		with(AlertDialog.Builder(requireContext())) {
			setTitle(R.string.error_title)
			setMessage(R.string.error_msg)
			show()
		}
	}

	override fun showRemovingFromFavourite(movie: Movie) {
		RemovingFavouriteDialog.newInstance(movie) {
			presenter.removeFromFavourite(movie)
		}.show(childFragmentManager)
	}

	override fun showRatingDialog(movie: Movie) {
		RatingMovieDialog.newInstance(movie) { rating ->
			presenter.confirmFavouriteMovie(movie, rating)
		}.show(childFragmentManager)
	}

	override fun onPrepareOptionsMenu(menu: Menu?) {
		super.onPrepareOptionsMenu(menu)
		searchMenuItem = menu?.findItem(R.id.menu_action_search)
		searchMenuItem?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
			override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
				return true
			}

			override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
				searchView?.setOnQueryTextListener(null)
				return true
			}
		})

		searchView = searchMenuItem?.actionView as SearchView?
		searchView?.setOnQueryTextFocusChangeListener(this::onFocusChangeListener)
	}

	private fun onFocusChangeListener(v: View, hasFocus: Boolean) {
		if (hasFocus) {
			presenter.checkLastQuery()
			initSearchQueryListener()
		}
	}

	override fun onDestroyOptionsMenu() {
		super.onDestroyOptionsMenu()
		searchMenuItem?.setOnActionExpandListener(null)
		searchMenuItem = null
		searchView?.setOnQueryTextFocusChangeListener(null)
		searchView = null
	}

	override fun emptyError() {
		showErrorInfo(getString(R.string.error_empty))
	}

	override fun movieNotFoundError() {
		showErrorInfo(getString(R.string.error_movie_not_found))
	}

	override fun tooManyResultError() {
		showErrorInfo(getString(R.string.error_too_many_result))
	}

	override fun internetConnectionError() {
		showErrorInfo(getString(R.string.error_internter_connection))
	}

	override fun validationError() {
		showErrorInfo(getString(R.string.error_validation))
	}

	private fun showErrorInfo(message: String) {
		search_movies_info.visibility = View.VISIBLE
		search_movies_list.visibility = View.GONE
		search_movies_info.text = message
	}

	private fun initSearchQueryListener() {
		searchView?.setOnQueryTextListener(DelayTextWatcher(SEARCH_DELAY_MS) { query ->
			searchView?.isQueryRefinementEnabled = false
			if (searchView != null) {
				presenter.loadMovies(true, query)
			}
		})
	}
}
