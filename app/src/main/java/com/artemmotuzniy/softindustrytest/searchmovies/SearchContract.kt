package com.artemmotuzniy.softindustrytest.searchmovies

import com.artemmotuzniy.softindustrytest.base.BasePresenter
import com.artemmotuzniy.softindustrytest.base.BaseView
import com.artemmotuzniy.softindustrytest.base.view.ErrorView
import com.artemmotuzniy.softindustrytest.base.view.InternetConnectionView
import com.artemmotuzniy.softindustrytest.base.view.LoaderView
import com.artemmotuzniy.softindustrytest.base.view.MovieListView
import com.artemmotuzniy.softindustrytest.data.model.Movie

interface SearchMoviesView : BaseView, MovieListView, LoaderView, ErrorView, InternetConnectionView {

    fun updateMovie(movie: Movie)

	fun showRatingDialog(movie: Movie)

	fun showRemovingFromFavourite(movie: Movie)

	fun movieNotFoundError()

	fun tooManyResultError()

	fun showLastQuery(currentQuery: String)

	fun validationError()

}

interface SearchMoviesPresenter : BasePresenter<SearchMoviesView> {

	fun loadMovies(refresh: Boolean = false)

	fun loadMovies(refresh: Boolean = false, query: String)

    fun updateMovie(movie: Movie)

	fun confirmFavouriteMovie(movie: Movie, rating: Float)

	fun removeFromFavourite(movie: Movie)

	fun checkLastQuery()

}