package com.artemmotuzniy.softindustrytest.searchmovies

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.RatingBar
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.artemmotuzniy.softindustrytest.R
import com.artemmotuzniy.softindustrytest.data.model.Movie
import kotlinx.android.synthetic.main.dialog_movie_rating.*
import kotlinx.android.synthetic.main.dialog_movie_rating.view.*

class RatingMovieDialog : DialogFragment() {

    companion object {
        private const val TAG = "RatingMovieDialog"
        private const val KEY_MOVIE = "key_movie"

        fun newInstance(movie: Movie, ratingListener: (Float) -> Unit): RatingMovieDialog {
            return RatingMovieDialog().apply {
                arguments = Bundle().apply {
                    putParcelable(KEY_MOVIE, movie)
                }
                this.ratingListener = ratingListener
            }
        }
    }

    private var ratingListener: ((Float) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val movie = arguments?.getParcelable<Movie>(KEY_MOVIE)
        val view = View.inflate(requireContext(), R.layout.dialog_movie_rating, null)

        return createDialog(view, movie)
    }

    private fun createDialog(view: View, movie: Movie?) = with(AlertDialog.Builder(requireContext())) {
        setView(view)
        setTitle(movie?.title)
        setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.dismiss()
        }
        setPositiveButton(android.R.string.ok) { dialog, _ ->
            ratingListener?.invoke(view.movie_rating_rating.rating)
            dialog.dismiss()
        }
        create()
    }

    fun show(childFragmentManager: FragmentManager) {
        show(childFragmentManager, TAG)
    }
}