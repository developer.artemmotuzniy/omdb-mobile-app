package com.artemmotuzniy.softindustrytest.searchmovies

import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.data.model.SearchMovieResult
import com.artemmotuzniy.softindustrytest.handleError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class SearchMoviesPresenterImpl(private val searchRepository: SearchRepository) : SearchMoviesPresenter {

    companion object {
        private const val PAGE_SIZE = 10
		private const val VALIDATION_PATTERN = "^$|^[a-zA-Z0-9\$@\$!%*?&#^-_.,\\- +]+\$"
    }

	override var view: SearchMoviesView? = null
    override var scope: CoroutineScope? = null

    private var page: Int = 1
    private var isLastPage = false
	private var lastQuery = ""

	override fun loadMovies(refresh: Boolean, query: String) {
		if (refresh) initLoadMovies(query) else nextLoadMovies(query)
	}

	override fun loadMovies(refresh: Boolean) {
		loadMovies(refresh, lastQuery)
	}

	private fun initLoadMovies(query: String) {
		scope?.launch {
			view?.showLoader()
			if (query != lastQuery) {
				lastQuery = query
				if (validationQuery(query)) {
					try {
						val response = withContext(Dispatchers.IO) {
							searchRepository.getMoviesWithFavourites(query, page = page)
						}
						page = 1
						isLastPage = checkLastPage(response)
						view?.setData(response.movies)
					} catch (e: Exception) {
						handleError(view, e)
					}
				} else {
					view?.validationError()
				}
			}
			view?.hideLoader()
		}


	}

	private fun validationQuery(query: String): Boolean {
		return Pattern.compile(VALIDATION_PATTERN).matcher(query).matches()
	}

	override fun checkLastQuery() {
		if (!lastQuery.isBlank()) {
			view?.showLastQuery(lastQuery)
		}
	}

	private fun checkLastPage(response: SearchMovieResult): Boolean {
		return response.totalResults <= PAGE_SIZE * page
	}

	private fun nextLoadMovies(query: String) {
        if (!isLastPage) {
			view?.showLoader()
			scope?.launch {
                try {
					page++
					val response = withContext(Dispatchers.IO) {
						searchRepository.getMoviesWithFavourites(query, page)
                    }

					isLastPage = checkLastPage(response)
					view?.addData(response.movies)
				} catch (e: Exception) {
					view?.showError()
				} finally {
					view?.hideLoader()
				}
            }
		}
    }

    override fun updateMovie(movie: Movie) {
		if (movie.favourite) view?.showRemovingFromFavourite(movie)
		else view?.showRatingDialog(movie)
	}

	override fun removeFromFavourite(movie: Movie) {
		scope?.launch {
			view?.showLoader()
			try {
				val updatedMovie = withContext(Dispatchers.IO) {
					searchRepository.removeMovieFromFavourite(movie)
				}
				view?.updateMovie(updatedMovie)
			} catch (e: Exception) {
				view?.showError()
			} finally {
				view?.hideLoader()
			}
		}
	}

	override fun confirmFavouriteMovie(movie: Movie, rating: Float) {
		scope?.launch {
			view?.showLoader()
			try {
				val updatedMovie = withContext(Dispatchers.IO) {
					if (movie.favourite) searchRepository.removeMovieFromFavourite(movie)
					else {
						movie.rating = rating
						searchRepository.addMovieToFavourite(movie)
					}
				}
				view?.updateMovie(updatedMovie)
			} catch (e: Exception) {
				view?.showError()
			} finally {
				view?.hideLoader()
			}
		}
	}
}