package com.artemmotuzniy.softindustrytest

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.setUpPagination(onNextPage: () -> Unit) {
	val layoutManager = layoutManager
	if (layoutManager is LinearLayoutManager) {
		addOnScrollListener(object : RecyclerView.OnScrollListener() {
			var firstVisible = 0

			override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
				super.onScrolled(recyclerView, dx, dy)
				val totalItemCount = layoutManager.itemCount
				val firstVisibleItemPosition = layoutManager.findLastVisibleItemPosition()

				if (firstVisible != firstVisibleItemPosition) {
					firstVisible = firstVisibleItemPosition

					if (totalItemCount - firstVisibleItemPosition == 3) {
						onNextPage()
					}
				}
			}
		})
	}
}