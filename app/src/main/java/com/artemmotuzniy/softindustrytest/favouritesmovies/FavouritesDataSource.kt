package com.artemmotuzniy.softindustrytest.favouritesmovies

import com.artemmotuzniy.softindustrytest.data.MoviesRepository
import com.artemmotuzniy.softindustrytest.data.model.Movie

class FavouritesDataSource(private val moviesRepository: MoviesRepository) : FavouritesRepository {

    override suspend fun getFavouriteMovies(offset:Int): List<Movie> {
        return moviesRepository.getFavouriteMovies(offset)
    }

    override suspend fun removeMovieFromFavourite(movie: Movie): Movie {
        return moviesRepository.removeMovieFromFavourite(movie)
    }
}