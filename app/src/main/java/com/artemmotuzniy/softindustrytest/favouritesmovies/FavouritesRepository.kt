package com.artemmotuzniy.softindustrytest.favouritesmovies

import com.artemmotuzniy.softindustrytest.data.model.Movie

interface FavouritesRepository {

	suspend fun getFavouriteMovies(offset: Int = 0): List<Movie>

	suspend fun removeMovieFromFavourite(movie: Movie): Movie

}