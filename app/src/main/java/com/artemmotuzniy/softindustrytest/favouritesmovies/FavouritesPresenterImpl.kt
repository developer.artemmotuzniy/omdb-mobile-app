package com.artemmotuzniy.softindustrytest.favouritesmovies

import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.handleError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavouritesPresenterImpl(private val favouritesRepository: FavouritesRepository) : FavouritePresenter {

    override var view: FavouriteView? = null
    override var scope: CoroutineScope? = null
	private var offset = 0

	override fun load(refresh: Boolean) {
		if (refresh) initLoad() else nextDataLoad()
	}

	private fun nextDataLoad() {
		scope?.launch {
			view?.showLoader()
			try {
				val response = withContext(Dispatchers.IO) {
					favouritesRepository.getFavouriteMovies(offset)
				}

				offset += response.size
				view?.addData(response)
			} catch (e: Exception) {
				handleError(view, e)
			} finally {
				view?.hideLoader()
			}
		}
	}

	private fun initLoad() {
		scope?.launch {
			view?.showLoader()
			try {
				val response = withContext(Dispatchers.IO) {
					favouritesRepository.getFavouriteMovies()
				}

				offset += response.size
				view?.setData(response)
			} catch (e: Exception) {
				handleError(view, e)
			} finally {
				view?.hideLoader()
			}
		}
	}

	override fun updateFavouriteState(movie: Movie) {
		view?.checkRemovingFromFavourite(movie)
	}

	override fun confirmRemove(movie: Movie) {
		scope?.launch {
			view?.showLoader()
			try {
				val response = withContext(Dispatchers.IO) {
					favouritesRepository.removeMovieFromFavourite(movie)
				}

				view?.remove(response)
			} catch (e: Exception) {
				view?.showError()
			} finally {
				view?.hideLoader()
			}
		}
	}
}