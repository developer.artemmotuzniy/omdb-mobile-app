package com.artemmotuzniy.softindustrytest.favouritesmovies

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.artemmotuzniy.softindustrytest.MoviesAdapter
import com.artemmotuzniy.softindustrytest.R
import com.artemmotuzniy.softindustrytest.RemovingFavouriteDialog
import com.artemmotuzniy.softindustrytest.data.model.Movie
import com.artemmotuzniy.softindustrytest.setUpPagination
import kotlinx.android.synthetic.main.fragment_favourites.*
import org.koin.android.ext.android.inject

class FavouriteFragment : Fragment(), FavouriteView {

	private val presenter: FavouritePresenter by inject()
	private val adapter = MoviesAdapter {
		presenter.updateFavouriteState(it)
	}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
		presenter.attach(this)

		favourite_movies_refresh.setOnRefreshListener {
			presenter.load(true)
		}

		favourite_movies_list.layoutManager = LinearLayoutManager(requireContext())
		favourite_movies_list.adapter = adapter
		favourite_movies_list.setUpPagination {
			presenter.load(false)
		}
	}

	override fun onDestroyView() {
		super.onDestroyView()
		presenter.detach()
	}

	override fun setData(movies: List<Movie>) {
		favourite_movies_info.visibility = View.GONE
		favourite_movies_list.visibility = View.VISIBLE

		adapter.setMovies(movies)
	}

	override fun addData(movies: List<Movie>) {
		adapter.addMovies(movies)
	}

	override fun remove(movie: Movie) {
		adapter.remove(movie)
	}

	override fun showLoader() {
		favourite_movies_refresh.isRefreshing = true
	}

	override fun hideLoader() {
		favourite_movies_refresh.isRefreshing = false
	}

	override fun checkRemovingFromFavourite(movie: Movie) {
		RemovingFavouriteDialog.newInstance(movie) {
			presenter.confirmRemove(movie)
		}.show(childFragmentManager)
	}

	override fun showError() {
		with(AlertDialog.Builder(requireContext())) {
			setTitle(R.string.error_title)
			setMessage(R.string.error_msg)
			show()
		}
	}

	override fun emptyError() {
		showErrorInfo(getString(R.string.error_empty))
	}

	override fun internetConnectionError() {
		showErrorInfo(getString(R.string.error_internter_connection))
	}

	private fun showErrorInfo(message: String) {
		favourite_movies_list.visibility = View.GONE
		favourite_movies_list.visibility = View.VISIBLE
		favourite_movies_info.text = message
	}

	override fun visible() {
		presenter.load(true)
	}
}
