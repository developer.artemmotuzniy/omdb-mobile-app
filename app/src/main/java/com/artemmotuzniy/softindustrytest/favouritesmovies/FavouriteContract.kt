package com.artemmotuzniy.softindustrytest.favouritesmovies

import com.artemmotuzniy.softindustrytest.base.view.InternetConnectionView
import com.artemmotuzniy.softindustrytest.base.BasePresenter
import com.artemmotuzniy.softindustrytest.base.BaseView
import com.artemmotuzniy.softindustrytest.base.view.ErrorView
import com.artemmotuzniy.softindustrytest.base.view.LoaderView
import com.artemmotuzniy.softindustrytest.base.view.MovieListView
import com.artemmotuzniy.softindustrytest.base.view.PageView
import com.artemmotuzniy.softindustrytest.data.model.Movie

interface FavouriteView : BaseView, MovieListView, LoaderView, ErrorView, PageView, InternetConnectionView {

	fun remove(movie: Movie)

	fun checkRemovingFromFavourite(movie: Movie)

}

interface FavouritePresenter : BasePresenter<FavouriteView>{

	fun load(refresh: Boolean)

	fun updateFavouriteState(movie: Movie)

	fun confirmRemove(movie: Movie)
}